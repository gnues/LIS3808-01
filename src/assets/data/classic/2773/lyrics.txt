﻿<Belle>
Belle 
C'est un mot qu'on dirait invente pour elle 
Quand elle danse et qu'elle met son corps a jour, tel 
Un oiseau qui etend ses ailes pour s'envoler 
Alors je sens l'enfer s'ouvrir sous mes pieds 
J'ai pose mes yeux sous sa robe de gitane 
A quoi me sert encore de prier Notre-Dame 
Quel 
Est celui qui lui jettera la premiere pierre 
Celui-la ne merite pas d'etre sur terre 
O Lucifer ! 
Oh ! Laisse-moi rien qu'une fois 
Glisser mes doigts dans les cheveux d'Esmeralda 

Belle 
Est-ce le diable qui s'est incarne en elle 
Pour detourner mes yeux du Dieu eternel 
Qui a mis dans mon etre ce desir charnel 
Pour m'empecher de regarder vers le Ciel 
Elle porte en elle le peche originel 
La desirer fait-il de moi un criminel 
Celle 
Qu'on prenait pour une fille de joie une fille de rien 
Semble soudain porter la croix du genre humain 
O Notre-Dame ! 
Oh ! laisse-moi rien qu'une fois 
Pousser la porte du jardin d'Esmeralda 

Belle 
Malgre ses grands yeux noirs qui vous ensorcellent 
La demoiselle serait-elle encore pucelle ? 
Quand ses mouvements me font voir monts et merveilles 
Sous son jupon aux couleurs de l'arc-en-ciel 
Ma dulcinee laissez-moi vous etre infidele 
Avant de vous avoir mene jusqu'a l'autel 
Quel 
Est l'homme qui detournerait son regard d'elle 
Sous peine d'etre change en statue de sel 
O Fleur-de-Lys, 
Je ne suis pas homme de foi 
J'irai cueillir la fleur d'amour d'Esmeralda 

Quasimodo, Frollo et Phœbus : 

J'ai pose mes yeux sous sa robe de gitane 
A quoi me sert encore de prier Notre-Dame 
Quel 
Est celui qui lui jettera la premiere pierre 
Celui-la ne merite pas d'etre sur terre 
O Lucifer ! 
Oh ! laisse-moi rien qu'une fois 
Glisser mes doigts dans les cheveux d'Esmeralda 
Esmeralda 

*종치기 콰지모도, 근위 대장 페뷔스, 성직자 프롤로 세 남자가 에스메랄다를 향한 사랑을
노래하는 곡. 프랑스 싱글 차트에서 무려 44주간이나 1위를 차지해 대기록을 세우기도 했다.