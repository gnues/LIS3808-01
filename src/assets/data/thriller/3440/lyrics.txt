﻿<The Ballad of Sweeney Todd>
Tobias 

Attend the tale of Sweeney Todd. 
His skin was pale and his eye was odd. 
He shaved the faces of gentlemen 
who never thereafter were heard of again. 
He trod a path that few have trod 
did Sweeney Todd 
the demon barber of fleet street. 
He kept a shop in London town. 
Of fancy clients and good renown 
and what if none of their souls were saved 
they went to their maker impecably shaved. 
By Sweeney, 
by Sweeney Todd 
the demon barber of fleet street. 

Company 

Swing your razor wide! 
Sweeney, hold it to the skies. 
Freely flows the blood of those who moralize. 
His needswere few, his room was bare. 
A lavabo and a fancy chair. 
A mug of suds, and a leather strop, 
an apron, a towel, a pail, and a mop. 
For neatness he deserves a nod, 
does Sweeney Todd, 
the demon barber of Fleet Street. 
Inconspicuous Sweeney was, 
quick, and quiet and clean he was. 
Back of his smile, under his word, 
Sweeney heard music that nobody heard. 
Sweeney pondered and Sweeney planned, 
like a perfect machine he planned, 
Sweeney was smooth, Sweeney was subtle, 
Sweeney would blink, and rats would scuttle 
Sweeney was smooth, Sweeney was subtle 
Sweeney would blink, and rats would scuttle 
Sweeney was smooth, Sweeney was subtle, 
Sweeney would blink, and rats would scuttle 
Sweeney was smooth, Sweeney was subtle, 
Sweeney would blink, and rats would scuttle 
Sweeney! Sweeney! Sweeney! Sweeney! 
Sweeney! 

Sweeney Todd 

Attend the tale of Sweeney Todd! 

Company 

Attend the tale of Sweeney Todd! 

Sweeney Todd 

He served a dark and avengeful God! 

Company 

He served a dark and avengeful God! 

Sweeney Todd 

What happened then, well that's the play, 
and he wouldn't want us to give it away... 

Company 

Not Sweeney 
Not Sweeney Todd 
The demon barber of Fleet... 
Street... 

*이발사 스위니 토드가 얼마나 많은 사람들의 목을 잘랐는지 이야기하는 뮤지컬 오프닝 곡
소름 끼치는 날카로운 금속음, 앞으로 전개될 끔찍한 이야기를 피해자인 마을 사람들이 남 얘기 하듯이 담담하게 노래하는 모습이 공포감을 유발한다. 