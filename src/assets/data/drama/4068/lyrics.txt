﻿<Seasons of love>
525600 minutes
525000 moments so dear
525600 minutes
How do you measure
measure a year
In daylights in sunsets
In midnights
in cups of coffee
In inches in miles
in laughter in strife
In 525600 minutes
How do you measure
a year in the life
How about love
How about love
How about love
Measure in love
Seasons of love
Seasons of love
525600 minutes
525000 journeys to plan
525600 minutes
How do you measure
the life of a woman or a man
In truths that she learned
Or in times that he cried
In bridges he burned
Or the way that she died
It’s time now to sing out
Those story never end 
Let's celebrate and
remember a year
in the life of friends
Remember the love
Remember the love
Remember the love
Measure in love
Seasons of love
Seasons of love

* 2막이 올라갈 때 모든 배우들이 나와 일렬로 서서 부르는 노래로, 뮤지컬 렌트의
철학을 함축적으로 담고 있다. 미국 드라마 Glee에서 추모곡으로 쓰이기도 했다. 