﻿엘리자벳 역을 맡은 옥주현은 뮤지컬 엘리자벳 20주년 기념공연 무대에 서서 각국 엘리자벳과 함께 ‘나는 나만의 것’을 불렀다.
초연 당시 제작사인 EMK가 괴악한 가격책정, 할인, 패키지세트를 내세워 관객들의 분노를 사기도 했다. 
재공연 때 “사랑과 죽음의 론도”라는 곡이 추가되었다. 
