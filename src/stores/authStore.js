export default {
  namespaced: true,
  state: {
    redirectionTimeoutId: null,
  },
  getters: {
  },
  mutations: {
    updateRedirectionTimeoutId(state, { redirectionTimeoutId }) {
      state.redirectionTimeoutId = redirectionTimeoutId;
    },
  },
  actions: {
  },
};
