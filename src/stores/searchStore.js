export default {
  namespaced: true,
  state: {
    currentDB: 'Play DB',
    pagePlayDB: 1,
    orderPlayDB: 'opening',
    pageTheMusic: 1,
    orderTheMusic: 'opening',
    year: new Date(),
    running: 'running',
    type: '전체',
  },
  getters: {
    page(state) {
      let currentPage;
      switch (state.currentDB) {
        default:
        case 'Play DB': {
          currentPage = state.pagePlayDB;
          break;
        }
        case 'The Music': {
          currentPage = state.pageTheMusic;
          break;
        }
      }
      return currentPage;
    },
    order(state) {
      let currentOrder;
      switch (state.currentDB) {
        default:
        case 'Play DB': {
          currentOrder = state.orderPlayDB;
          break;
        }
        case 'The Music': {
          currentOrder = state.orderTheMusic;
          break;
        }
      }
      return currentOrder;
    },
  },
  mutations: {
    updateType(state, { type }) {
      state.type = type;
    },
    updateRunning(state, { running }) {
      state.running = running;
    },
    updateYear(state, { year }) {
      state.year = year;
    },
    updateCurrentDB(state, { currentDB }) {
      state.currentDB = currentDB;
    },
    updatePage(state, { page }) {
      switch (state.currentDB) {
        default:
        case 'Play DB': {
          state.pagePlayDB = page;
          break;
        }
        case 'The Music': {
          state.pageTheMusic = page;
          break;
        }
      }
    },
    updateOrder(state, { order }) {
      switch (state.currentDB) {
        default:
        case 'Play DB': {
          state.orderPlayDB = order;
          break;
        }
        case 'The Music': {
          state.orderTheMusic = order;
          break;
        }
      }
    },
  },
  actions: {
  },
};
