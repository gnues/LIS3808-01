export default {
  namespaced: true,
  state: {
    currentLocal: '서울',
    page: {},
  },
  getters: {
    getPage(state) {
      return state.page[state.currentLocal];
    },
  },
  mutations: {
    updateCurrentLocal(state, { currentLocal }) {
      state.currentLocal = currentLocal;
    },
    updatePage(state, { page }) {
      const tempPage = {
        ...state.page,
      };
      tempPage[state.currentLocal] = page;
      state.page = tempPage;
    },
  },
  actions: {
  },
};
